from string import ascii_letters
import sys
import random
import os


LOW_TRH = 8
HIGH_TRH = 32
SOURCE_STR = ascii_letters + "1234567890!@#$%^&*()-_=+<>,.?|"
OUTPUT_PATH = "output"


def gen_password(n):
    if not LOW_TRH < n < HIGH_TRH:
        return "n is too small or too big"  
    
    return "".join([random.choice(SOURCE_STR) for _ in range(n)])


if __name__ == "__main__":
    passw = gen_password(int(sys.argv[1]))

    os.makedirs(OUTPUT_PATH, exist_ok=True)
    with open(os.path.join(OUTPUT_PATH, "passw.txt"), "w+") as file:
        file.write(passw)