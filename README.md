# Часть 1. Задание 1.
## Установка:
1) Установите пакет через pip: `pip install project_password --extra-index-url https://gitlab.com/api/v4/projects/41739990/packages/pypi/simple --no-deps`;

## Выполнение:
В корневой папке выполните команду `python3 -m project_password.gen_pass $VAR` (укажите необходимую длину пароля от 8 до 32 вместо `$VAR`), результат будет находиться в текстовом файле `output/result.txt`.